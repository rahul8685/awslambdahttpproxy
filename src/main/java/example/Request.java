package example;

public class Request {
    String input;

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public Request(String input) {
        this.input = input;
    }

    public Request() {
    }
}