package example;

public class Response {
    String output;

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public Response(String output) {
        this.output = output;
    }

    public Response() {
    }
}