package example;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;

import java.io.*;
import java.net.*;
import java.util.Enumeration;

public class Hello implements RequestHandler<Request, Response> {

    private String input;
    private LambdaLogger logger;

    private void getInterfaces() {
        try {
            InetAddress localhost = InetAddress.getLocalHost();
            this.logger.log(" IP Addr: " + localhost.getHostAddress());
            // Just in case this host has multiple IP addresses....
            InetAddress[] allMyIps = InetAddress.getAllByName(localhost.getCanonicalHostName());
            if (allMyIps != null && allMyIps.length > 1) {
                this.logger.log(" Full list of IP addresses:");
                for (InetAddress allMyIp : allMyIps) {
                    this.logger.log("    " + allMyIp);
                }
            }
        } catch (UnknownHostException e) {
            this.logger.log(" (error retrieving server host name)");
        }

        try {
            this.logger.log("Full list of Network Interfaces:");
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                this.logger.log("    " + intf.getName() + " " + intf.getDisplayName());
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    this.logger.log("        " + enumIpAddr.nextElement().toString());
                }
            }
        } catch (SocketException e) {
            this.logger.log(" (error retrieving network interface list)");
        }
    }

    private String runCommand() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        CommandLine commandline = CommandLine.parse(input);
        DefaultExecutor exec = new DefaultExecutor();
        PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
        exec.setStreamHandler(streamHandler);
        exec.setExitValues(null);
        int exitValue = 0;
        try {
            exitValue = exec.execute(commandline);
        } catch (IOException e) {
            logger.log("Exception: " + e.getMessage());
        }
        return String.format("exitValue=%d\n%s", exitValue, outputStream.toString());
    }

    // HTTP GET request
    private String sendGet() throws Exception {

        String url = "http://api.ipify.org/";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        String USER_AGENT = "Mozilla/5.0";
        con.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();
        this.logger.log("URL: " + url);
        this.logger.log("Response Code: " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        return response.toString();

    }

    @Override
    public Response handleRequest(Request request, Context context) {
        this.input = request.input;
        this.logger = context.getLogger();
        this.logger.log("received : " + input);
        String ip;
        try {
            ip = this.sendGet();
        } catch (Exception e) {
            this.logger.log("Exception: " + e.getMessage());
            ip = "Not found";
        }
        this.getInterfaces();
        return new Response(String.format("ip=%s\n%s", ip, runCommand()));
    }
}